
export default class Tax {
    /**
     * Applies tax to list price
     * @param {number} list_price before tax
     * @param {number} tax in percentage
     * @returns {number} price with tax (total_price)
     */
    static applyTax(list_price, tax) {
        const tax_rate = tax / 100;
        const after_tax = list_price + list_price * tax_rate;
        return after_tax;
    }
    /**
     * Returns price without tax
     * @param {number} total_price after tax
     * @param {number} tax in percentage
     * @returns {number} price before tax
     */
    static deductTax(total_price, tax) {
        const tax_rate = 1 + (tax / 100);
        const list_price = total_price / tax_rate;
        return list_price;
    }
    /**
     * Figures out tax percentage from before and after prices
     * @param {number} list_price before tax
     * @param {number} total_price after tax
     * @returns {number} tax percentage
     */
    static figureTaxRate(list_price, total_price) {
        const tax_rate = (1 / (list_price / total_price)) - 1;
        const tax_rate_percent = tax_rate * 100;
        return tax_rate_percent;
    }
}
